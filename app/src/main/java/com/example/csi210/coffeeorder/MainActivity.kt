package com.example.csi210.coffeeorder

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            buy_button.setOnClickListener {
                nav_host_fragment.findNavController().navigate(R.id.action_global_coffeeFragment)
            }
            cart_button.setOnClickListener {
                nav_host_fragment.findNavController().navigate(R.id.action_global_cartFragment)
            }
        }
    }
}
